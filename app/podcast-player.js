import React from 'react';

export default React.createClass({
	render: function() {
		return <audio  controls src={this.props.src}></audio>
	}
});