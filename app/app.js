
import ReactDOM from 'react-dom';
import React from 'react';

import PodcastPlayer from './podcast-player';

ReactDOM.render(
	<PodcastPlayer src="http://v.giantbomb.com/podcast/Giant_Bombcast_11_17_2015-11-17-2015-4768141922.mp3" />,
	document.getElementById('root')
);
